class Potepan::ProductsController < ApplicationController
  def index
  end

  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.taxons.map{|taxon|taxon.products.where.not(id: @product.id)}.flatten.uniq.sample(4)
  end
end
